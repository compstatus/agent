package disk

import (
	"syscall"
)

const (
	B  = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

type FileSystemStatus struct {
	MountPoint string  `json:"mount_point"`
	All        float64 `json:"all"`
	Used       float64 `json:"used"`
	Free       float64 `json:"free"`
}

func (o *FileSystemStatus) Update() error {
	fs := syscall.Statfs_t{}
	err := syscall.Statfs(o.MountPoint, &fs)
	if err != nil {
		return err
	}
	all := fs.Blocks * uint64(fs.Bsize)
	free := fs.Bfree * uint64(fs.Bsize)
	used := all - free
	o.All = float64(all) / float64(GB)
	o.Used = float64(used) / float64(GB)
	o.Free = float64(free) / float64(GB)
	return nil
}
