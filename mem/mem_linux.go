package mem

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

var (
	memTotalRe     = regexp.MustCompile(`^MemTotal:\s+([0-9]+)\s+[kK]B$`)
	memFreeRe      = regexp.MustCompile(`^MemFree:\s+([0-9]+)\s+[kK]B$`)
	memAvailableRe = regexp.MustCompile(`^MemAvailable:\s+([0-9]+)\s+[kK]B$`)
)

func (o *Mem) Update() error {
	file, err := os.Open("/proc/meminfo")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		matchesAll := memTotalRe.FindSubmatch(scanner.Bytes())
		if len(matchesAll) > 0 {
			AllInKb, err := strconv.ParseUint(string(matchesAll[1]), 10, 64)
			if err == nil {
				o.All = float64(AllInKb*KB) / float64(GB)
			}
		}
		matchesFree := memFreeRe.FindSubmatch(scanner.Bytes())
		if len(matchesFree) > 0 {
			freeInKb, err := strconv.ParseUint(string(matchesFree[1]), 10, 64)
			if err == nil {
				o.Free = float64(freeInKb*KB) / float64(GB)
			}
		}
		matchesAvailable := memAvailableRe.FindSubmatch(scanner.Bytes())
		if len(matchesAvailable) > 0 {
			availableInKb, err := strconv.ParseUint(string(matchesAvailable[1]), 10, 64)
			if err == nil {
				o.Available = float64(availableInKb*KB) / float64(GB)
			}
		}
		o.Used = o.All - o.Free

	}
	return nil
}
